﻿using societeTableau.Structures;
using System;
using System.Collections.Generic;

namespace societeTableau
{
    class Program
    {
        static void Main(string[] args)
        {
            var tab = new List<Personne>();

            tab.Add(new Employes(1200, "Stern", "Alex", 20));
            tab.Add(new Employes(1580, "Starkov", "Alina", 26));
            tab.Add(new Employes(1800, "Potter", "Harry", 27));
            tab.Add(new Employes(1540, "Wilde", "Ivy", 30));
            tab.Add(new Employes(1300, "Oretsev", "Mal", 26));
            tab.Add(new Chef("Informatique",2100, "Arlington", "Daniel", 24 ));
            tab.Add(new Chef("Ressources humaines",1800, "Summer", "Buffy", 25 ));
            tab.Add(new Directeur("Poudlar","Ressources humaines",3000, "Dumbledore", "Albus", 60 ));

            for (int i = 0; i < tab.Count; i++)
            {
                tab[i].Afficher();
            }

            foreach (var personne in tab)
            {
                personne.Afficher();
            }
        }
    }
}
