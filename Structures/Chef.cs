﻿using System;
using System.Collections.Generic;
using System.Text;

namespace societeTableau.Structures
{
    public class Chef : Employes
    {
        public string Service { get; set; }

        public Chef(string service, double salaire, string nom, string prenom, int age) : base(salaire, nom, prenom, age)
        {
            this.Service = service;
        }

        public override string ToString()
        {
            return $"{this.Nom} {this.Prenom} {this.Age} {this.Salaire} {this.Service}";
        }

        public void Afficher(Chef chef)
        {
            Console.WriteLine($"Nom:{chef.Nom}");
            Console.WriteLine($"Prenom:{chef.Prenom}");
            Console.WriteLine($"Age:{chef.Age}");
            Console.WriteLine($"Salaire:{chef.Salaire}");
            Console.WriteLine($"Service:{chef.Service}");
            Console.WriteLine("");


        }

        public void Afficher()
        {
            Console.WriteLine($"Nom:{this.Nom}");
            Console.WriteLine($"Prenom:{this.Prenom}");
            Console.WriteLine($"Age:{this.Age}");
            Console.WriteLine($"Salaire:{this.Salaire}");
            Console.WriteLine($"Service:{this.Service}");
            Console.WriteLine("");


        }
    }
}
