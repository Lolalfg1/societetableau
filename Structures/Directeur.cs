﻿using System;
using System.Collections.Generic;
using System.Text;

namespace societeTableau.Structures
{
    public class Directeur : Chef
    {
        public string Societe { get; set; }


        public Directeur(string societe, string service, double salaire, string nom, string prenom, int age) : base(service, salaire, nom, prenom, age)
        {
            this.Societe = societe;
        }

        public override string ToString()
        {
            return $"{this.Nom} {this.Prenom} {this.Age} {this.Salaire} {this.Service} {this.Societe}";
        }

        public void Afficher(Directeur directeur)
        {
            Console.WriteLine($"Nom:{directeur.Nom}");
            Console.WriteLine($"Prenom:{directeur.Prenom}");
            Console.WriteLine($"Age:{directeur.Age}");
            Console.WriteLine($"Salaire:{directeur.Salaire}");
            Console.WriteLine($"Service:{directeur.Service}");
            Console.WriteLine($"Societe:{directeur.Societe}");
            Console.WriteLine("");


        }

        public void Afficher()
        {
            Console.WriteLine($"Nom:{this.Nom}");
            Console.WriteLine($"Prenom:{this.Prenom}");
            Console.WriteLine($"Age:{this.Age}");
            Console.WriteLine($"Salaire:{this.Salaire}");
            Console.WriteLine($"Service:{this.Service}");
            Console.WriteLine($"Societe:{this.Societe}");
            Console.WriteLine("");


        }
    }
}
